# Titulo del proyecto

Descripción del software, enfocandose en el propósito su desarrollo.

## Clonación rápida

Par iniciar el desarrollo con este proyecto debe clonar localmente el repositorio.

`git clone URL del repositorio`

## Requisitos previos

- Para clonar el repositorio se necesita tener instalado git. Se puede obtener git haciendo clic [aquí](https://git-scm.com/)

- Se hace uso de unas cuantas herramientas de Node.js para compilar y ejecutar el proyecto. Se debe tener Node.js y su gestor de paquetes instalados. Para obtenerlos hacer clic [aquí](https://nodejs.org/en/).

- Bower. Se puede obtener haciendo clic [aquí](https://bower.io/)

- Gulp. Se puede obtener haciendo clic [aquí](https://gulpjs.com/docs/en/getting-started/quick-start)

## Despliegue

### Local

- Ejecutar los siguientes comandos

~~~
npm install
bower install

#En el caso de que estén definidas estas tareas en el gulpfile

gulp build-desa
~~~

### Certificación

- Ejecutar los siguientes comandos

~~~
npm install
bower install

#En el caso de que estén definidas estas tareas en el gulpfile

gulp build-cert
gulp deploy-cert
~~~

### Producción

- Ejecutar los siguientes comandos

~~~
npm install
bower install

#En el caso de que estén definidas estas tareas en el gulpfile

gulp build-prod
gulp deploy-cert
~~~

En el caso de que no estén definidas las tareas de despliegue en el gulpfile indicar la ruta de la aplicación en cada uno de los ambientes para hacer el despliegue manual.

## Desarrollado con

Se deben listar las tecnologías que se utilizaron para el desarrollo del software.

## Comentarios

Si se desea hacer una advertencia o hacer un recordatorio de algo se debe escribir en esta sección.

## Ayuda adicional

Para obtener mas ayuda vaya al sitio web oficial de [AngularJS](https://angularjs.org/)