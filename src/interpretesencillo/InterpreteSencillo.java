//Automatas y lenguajes Formales
//Billy Oswaldo Raquec Arias    Carnet 5990-14-5609
//Javier Francisco Sanchez de Leon  Carnet: 5990-06-1924
//Clase InterpreteSencillo
//Paquete

package interpretesencillo;
//imports
import arbol.Instruccion;   //importando instruccion
import arbol.TablaDeSimbolos;   //importando TablaDeSimbolos
import java.io.FileReader;  //importando FileReader
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;   //importando una LinkedList
import java.util.Scanner;
/**
 * Clase principal de la aplicación
 */
public class InterpreteSencillo {

    /**
     * Método principal de la aplicación
     * @param args los argumentos de la línea de comando
     */
    
    public static TablaDeSimbolos ts;
    public static Archivo errores;
    public static DateFormat horaFecha = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");   //objeto para la fecha y hora
    public static Date fecha = new Date();                                              //objeto para la hora
    
    public static void main(String[] args) throws Exception {
        
        errores=new Archivo("./errores.html");
        errores.eliminar();
        errores.crearHtml("Errores");
        
        if ("compilar".equals(args[0])) {                         //si el argumento 0 es compilar
            
        } //fin if
        else if ("ejecutar".equals(args[0])) {                    //sino si es ejecutar
            if ("-d".equals(args[1])) {                           //si el argumento 1 es -d
                debugger.Debugger debugger = new debugger.Debugger(args[2]);      //crear un nuevo debugger con el argumento 2
                
                debugger.imprimirBienvenida();                  //imprimir bienvenida al debugger
                while (!"stop".equals(debugger.comando)) {        //mientras no se digite stop
                    debugger.solicitarComando();                //solicitar comando de debugger
                    debugger.reconocerComando();                //reconocer comando
                }                                               //fin while*/
            } //fin if
            else {                                               //si no 
                
            }                                                   //fin else
        } //fin else
        else if ("interpretar".equals(args[0])) {                 //sino si argumento 0 es interpretar
            interpretar(args[1]);
        }                                                       //fin else if
        
        errores.finalizarhtml();
    }
    /**
     * Método que lee el contenido de un archivo de texto y ejecuta las 
     * instrucciones que contiene.
     * @param path ruta del archivo de texto que se desea interpretar
     */
    public static void interpretar(String path) throws IOException {
        analizadores.Sintactico pars;
        LinkedList<Instruccion> AST_arbolSintaxisAbstracta=null;
        try {
            FileReader fr = new FileReader(path);
            pars=new analizadores.Sintactico(new analizadores.Lexico(fr));
            pars.parse();        
            AST_arbolSintaxisAbstracta=pars.getAST();
        } catch (Exception ex) {
            System.out.println(horaFecha.format(fecha));
            System.out.println("Error fatal en compilación de entrada.");
            errores.imprimir(horaFecha.format(fecha)+"<br>");
            errores.imprimir("Error fatal en compilación de entrada.<br>");
        } 
        ejecutarAST(AST_arbolSintaxisAbstracta);
    }
    /**
     * Recibe una lista de instrucciones y la ejecuta
     * @param ast lista de instrucciones
     */
    private static void ejecutarAST(LinkedList<Instruccion> ast) throws IOException {
        if(ast==null){
            System.out.println(horaFecha.format(fecha));
            System.out.println("No es posible ejecutar las instrucciones porque\r\n"
                    + "el árbol no fue cargado de forma adecuada por la existencia\r\n"
                    + "de errores léxicos o sintácticos.");
            errores.imprimir(horaFecha.format(fecha)+"<br>");
            errores.imprimir("No es posible ejecutar las instrucciones porque\r\n"
                    + "el árbol no fue cargado de forma adecuada por la existencia\r\n"
                    + "de errores léxicos o sintácticos.<br>");
            return;
        }
        //Se crea una tabla de símbolos global para ejecutar las instrucciones.
        ts=new TablaDeSimbolos();
        //Se ejecuta cada instruccion en el ast, es decir, cada instruccion de 
        //la lista principal de instrucciones.
        for(Instruccion ins:ast){
            //Si existe un error léxico o sintáctico en cierta instrucción esta
            //será inválida y se cargará como null, por lo tanto no deberá ejecutarse
            //es por esto que se hace esta validación.
            if(ins!=null)
                ins.ejecutar(ts);
        }
    }
}
