//Automatas y lenguajes Formales
//Billy Oswaldo Raquec Arias    Carnet 5990-14-5609
//Javier Francisco Sanchez de Leon  Carnet: 5990-06-1924
//Clase Archivo
//Paquete
package interpretesencillo;
//imports
//imports para manejos de archivos

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Archivo {   //inicia clase Archivo se usara para manejo de archivos

    private final String path;        //contendra la direccion del archivo a manipular

    public Archivo(String path) {    //Constructor que tomara la direccion del archivo
        this.path = path;             //asignando path
    }                               //fin constructor

    public void eliminar() {         //metodo eliminar que eliminara el archivo 
        File archivo = new File(path);   //abriendo el archivo
        archivo.delete();               //eliminando archivo
    }                                   //fin metodo eliminar

    public void crearHtml(String titulo) throws IOException {   //metodo crearHtml Creara un nuevo archivo HTML recivira como parametro el titulo de la pagina web
        File archivo = new File(path);                          //abriendo archivo
        archivo.createNewFile();                                //creando nuevo archivo
        FileWriter fw = new FileWriter(archivo, true);           //un nuevo escritor de archivo
        try (PrintWriter pw = new PrintWriter(fw)) {            //try            
            //agregando contenido al archivo html
            pw.append("<!DOCTYPE html>\n");
            pw.append("<html lang=\"en\">\n");
            pw.append("<html>\n");
            pw.append("<head>\n");
            pw.append("\t<meta charset=\"UTF-8\">\n");
            pw.append("\t<title>" + titulo + "</title>\n");
            pw.append("\t<meta name=\"viewport\" content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.9, minimum-scale=1.0\">\n");
            pw.append("\t<link rel=\"stylesheet\" href=\"../css/bootstrap.min.css\">\n");
            pw.append("\t<link rel=\"stylesheet\" type=\"text/css\" href=\"../css/estilos.css\"/>\n");
            pw.append("</head>\n");
            pw.append("<body>\n");
            pw.append("\t<header>\n");
            pw.append("\t\t<div class=\"container\">\n");
            pw.append("\t\t\t<div class=\"row\">\n");
            pw.append("\t\t\t\t<div class = \"col-xs-12 col-sm-8 col-md-9 col-lg-9\">\n");
            pw.append("\t\t\t\t\t<h1>Universidad Mariano Galvez de Guatemala</h1>\n");
            pw.append("\t\t\t\t\t<h2>Centro Universitario Chinautla</h2>\n");
            pw.append("\t\t\t\t</div>\n");
            pw.append("\t\t\t\t<div class = \"col-xs-12 col-sm-4 col-md-3 col-lg-3\">\n");
            pw.append("\t\t\t\t\t<img src=\"../imagenes/images.png\" width=\"125px\">\n");
            pw.append("\t\t\t\t</div>\n");
            pw.append("\t\t\t</div>\n");
            pw.append("\t\t</header>\n");
            pw.append("\t\t<div class=\"container\">\n");
            pw.append("\t\t\t<section class=\"main-row\">\n");
            pw.append("\t\t\t\t<article class=\"col-xs-12 col-sm-8 col-md-9 col-lg-9\">\n");
            pw.append("\t\t\t\t\t<p>\n");
            pw.close();
        }   //fin try
    }       //fin metodo crearHtml

    public void finalizarhtml() throws IOException {   //Metodo finalizarhtml, imprimira en el archivo html las ultimas etiquetas de cierre
        File archivo = new File(path);              //abriendo archivo
        FileWriter fw = new FileWriter(archivo, true);   //nuevo escritor
        try (PrintWriter pw = new PrintWriter(fw)) {    //try
            //imprimiendo etiquetas de cierre
            pw.append("\t\t\t\t\t</p>\n");
            pw.append("\t\t\t\t</article>\n");
            pw.append("\t\t\t\t<aside class=\"col-xs-12 col-sm-4 col-md-3 col-lg-3\">\n");
            pw.append("\t\t\t\t\t<h3>Integrantes</h3>\n");
            pw.append("\t\t\t\t\t<p>\n");
            pw.append("\t\t\t\t\tBilly Oswaldo Raquec Arias <br>\n");
            pw.append("\t\t\t\t\tJavier Francisco Sanches de Leon <br>\n");
            pw.append("\t\t\t\t\t</p>\n");
            pw.append("\t\t\t\t</aside>\n");
            pw.append("\t\t\t</section>\n");
            pw.append("\t\t</div>\n");
            pw.append("\t\t<div class=\"container\">\n");
            pw.append("\t\t\t<section class=\"main-row\">\n");
            pw.append("\t\t\t\t<footer class=\"col-xs-12 col-sm-12 col-md-12 col-lg-12\">\n");
            pw.append("\t\t\t\t\t<p>\n");
            pw.append("\t\t\t\t\t\tCuando ejecute otra vez la aplicacion se borrará lo que se escribió en este archivo.\n");
            pw.append("\t\t\t\t\t</p>\n");
            pw.append("\t\t\t\t</article>\n");
            pw.append("\t\t\t</section>\n");
            pw.append("\t\t</div>\n");
            pw.append("</body>\n");
            pw.append("</html>\n");
            pw.close();
        }   //fin try
    }       //fin metodo finalizarhtml

    public void imprimir(String texto) throws IOException{
        File archivo = new File(path);              //abriendo archivo
        FileWriter fw = new FileWriter(archivo, true);   //nuevo escritor
        try (PrintWriter pw = new PrintWriter(fw)) {    //try
            pw.append(texto);
            pw.close();
        }   //fin try
    }
    
    public void crearArchivo() throws IOException{
        File archivo = new File(path);                          //abriendo archivo
        archivo.createNewFile();
    }
}           //fin clase Archivo
