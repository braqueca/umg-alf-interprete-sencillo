//Automatas y lenguajes Formales
//Billy Oswaldo Raquec Arias    Carnet 5990-14-5609
//Javier Francisco Sanchez de Leon  Carnet: 5990-06-1924
//Clase Case que implementa Instruccion
//Paquete
package arbol;

import java.util.LinkedList;

/**
 * Clase que tendra las instrucciones de cada case
 * @author billyraquec
 */
public class Case implements Instruccion {
    //lista de las instrucciones a ejecutar
    private final LinkedList<Instruccion> instrucciones;
    public final String valor;   //valor con el cual comparar
    
    /**
     * Constructor
     * @param valor Valor a comparar
     * @param instrucciones instrucciones a ejecutar
     */
    
    public Case(String valor, LinkedList<Instruccion> instrucciones){
        this.instrucciones=instrucciones;
        this.valor=valor;
    }
    
    /**
     * Metodo ejecutar que ejecuta las acciones en caso de tener una coincidencia
     * con el valor
     * @param ts tabla de simbolos
     * @return retorna null porque solo ejecuta las instrucciones
     */

    @Override
    public Object ejecutar(TablaDeSimbolos ts) {
        TablaDeSimbolos tablaLocal=new TablaDeSimbolos();
        tablaLocal.addAll(ts);
        for(Instruccion ins:instrucciones){
            System.out.println("Llego hasta aqui");
            ins.ejecutar(tablaLocal);
        }
        return null;
    }
}
