//Automatas y lenguajes Formales
//Billy Oswaldo Raquec Arias    Carnet 5990-14-5609
//Javier Francisco Sanchez de Leon  Carnet: 5990-06-1924
//Clase Switch que implementa Instruccion
//Paquete

package arbol;

import java.util.LinkedList;

/**
 * Clase que ejecuta la instruccion switch
 * @author billyraquec
 */
public class Switch implements Instruccion {
    //variable sobre la cual se va a hacer el switch
    private final String variable;
    private final LinkedList<Instruccion> caselist;   //lista de cases
    
    /**
     * Constructor
     * @param variable Variable con la que se va a evaluar el valor de cada case
     * @param caselist lista de cases
     */
    
    public Switch(String variable, LinkedList<Instruccion>caselist){
        this.variable=variable;
        this.caselist=caselist;
    }
    
    
    /**
     * Metodo ejecutar ejecuta todos los cases que coinciden con el valor de la
     * variable
     * @param ts tabla de simbolos
     * @return retorna null ya que solo ejecuta las acciones
     */

    @Override
    public Object ejecutar(TablaDeSimbolos ts) {
        TablaDeSimbolos tablaLocal=new TablaDeSimbolos();
        tablaLocal.addAll(ts);
        for(Instruccion ins:caselist){
            if(Double.parseDouble(tablaLocal.getValor(variable).toString())==Double.parseDouble(((Case)ins).valor)){
                ins.ejecutar(tablaLocal);
            }
        }
        return null;
    }
    
}
