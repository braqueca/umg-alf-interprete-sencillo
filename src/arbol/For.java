//Automatas y lenguajes Formales
//Billy Oswaldo Raquec Arias    Carnet 5990-14-5609
//Javier Francisco Sanchez de Leon  Carnet: 5990-06-1924
//Clase For
//Paquete
package arbol;

import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que ejecuta las acciones de una instrucción for y que implementa
 * la interfaz de instrucción
 */

public class For implements Instruccion{
    
    private final Asignacion asignacionInicial;   //asignacion inicial
    private final Operacion condicion;          //condicion de detencion
    private final Asignacion asignacionCambio;   //asignacion de cambio
    
    /**
     * Lista de las instrucciones que deben ejecutarse si la condición se cumple.
     */
    private final LinkedList<Instruccion> listaInstrucciones;
    
    /**
     * Constructor de la clase For
     * @param asignacionInicial asignacion inicial del for
     * @param condicion Condicion que se evalua para detencion
     * @param asignacionCambio cambio que tendra la variable
     * @param listaInstrucciones instrucciones a ejecutarse si se cumple la condicion
     */
    public For(Asignacion asignacionInicial, Operacion condicion, Asignacion asignacionCambio, LinkedList<Instruccion> listaInstrucciones){
        this.asignacionInicial=asignacionInicial;
        this.condicion=condicion;
        this.asignacionCambio=asignacionCambio;
        this.listaInstrucciones=listaInstrucciones;
    }
    
    /**
     * Método que ejecuta la instrucción for, es una sobreescritura del 
     * método ejecutar que se debe programar por la implementación de la interfaz
     * instrucción
     * @param ts tabla de símbolos del ámbito padre de la sentencia
     * @return Esta instrucción retorna nulo porque no produce ningun valor
     */
    
    @Override
    public Object ejecutar(TablaDeSimbolos ts) {
        try{
            asignacionInicial.ejecutar(ts);
            for(Double i=Double.parseDouble(asignacionInicial.valor.ejecutar(ts).toString()); Boolean.valueOf(condicion.ejecutar(ts).toString());i++){
                TablaDeSimbolos tablaLocal=new TablaDeSimbolos();
                tablaLocal.addAll(ts);
                for(Instruccion ins:listaInstrucciones){
                    ins.ejecutar(tablaLocal);
                }
                asignacionCambio.ejecutar(ts);
            }
        }catch(Exception ex){
            System.err.println(interpretesencillo.InterpreteSencillo.horaFecha.format(interpretesencillo.InterpreteSencillo.fecha));
            System.err.println("Se Produjo una excepcion debido a un error anterior");
            try {
                interpretesencillo.InterpreteSencillo.errores.imprimir(interpretesencillo.InterpreteSencillo.horaFecha.format(interpretesencillo.InterpreteSencillo.fecha)+"<br>");
                interpretesencillo.InterpreteSencillo.errores.imprimir("Se Produjo una excepcion debido a un error anterior<br>");
            } catch (IOException ex1) {
                Logger.getLogger(Mientras.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return null;
    }
    
}
