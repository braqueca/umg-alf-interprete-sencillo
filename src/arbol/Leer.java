//Automatas y lenguajes Formales
//Billy Oswaldo Raquec Arias    Carnet 5990-14-5609
//Javier Francisco Sanchez de Leon  Carnet: 5990-06-1924
//Clase Leer que implementa la interfaz Instruccion
//Paquete

package arbol;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Scanner;

/**
 * Clase que ejecuta las acciones de la instruccion leer y que implementa la 
 * interfaz Instruccion
 * @author billyraquec
 */
public class Leer implements Instruccion {

    /**
     * Identificador de la variable a la que se le asigna el valor.
     */
    private final String id;
    /**
     * Scanner para recibir el valor
     */
    
    private Scanner scanner = new Scanner(System.in);

    /**
     * Constructor de la clase asignación
     * @param id identificador de la variable
     */
    
    public Leer(String id){
        this.id=id;
    }
    
    /**
     * Método que ejecuta la accion de asignar un valor, es una sobreescritura del 
     * método ejecutar que se debe programar por la implementación de la interfaz
     * instrucción
     * @param ts tabla de símbolos del ámbito padre de la sentencia asignación
     * @return En este caso retorna nulo porque no es una sentencia que genere un valor.
     */
    
    @Override
    public Object ejecutar(TablaDeSimbolos ts) {
        ts.setValor(id,(Object)this.scanner.next());
        return null;
    }
    
}
