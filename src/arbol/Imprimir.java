//Automatas y lenguajes Formales
//Billy Oswaldo Raquec Arias    Carnet 5990-14-5609
//Javier Francisco Sanchez de Leon  Carnet: 5990-06-1924
//Clase Imprimir que implementa la interfaz Instruccion
//Paquete


package arbol;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que ejecuta las acciones de una instrucción imprimir y que implementa
 * la interfaz de instrucción
 */
public class Imprimir implements Instruccion{
    /**
     * Contenido que será impreso al ejecutar la instrucción imprimir, este debe
     * ser una instrucción que genere un valor al ser ejecutada.
     */
    private final Instruccion contenido;
    /**
     * Constructor de la clase imprimir
     * @param contenido contenido que será impreso al ejecutar la instrucción
     */
    public Imprimir(Instruccion contenido) {
        this.contenido = contenido;
    }
    /**
     * Método que ejecuta la accion de imprimir un valor, es una sobreescritura del 
     * método ejecutar que se debe programar por la implementación de la interfaz
     * instrucción
     * @param ts Tabla de símbolos del ámbito padre de la sentencia.
     * @return Esta instrucción retorna nulo porque no produce ningun valor al ser
     * ejecutada.
     */
    @Override
    public Object ejecutar(TablaDeSimbolos ts) {
        try{
            System.out.println(contenido.ejecutar(ts).toString().replace("\\", ""));
            return null;
        }catch(Exception ex){
            System.err.println(interpretesencillo.InterpreteSencillo.horaFecha.format(interpretesencillo.InterpreteSencillo.fecha));
            System.err.println("Se Produjo una excepcion debido a un error anterior");
            try {
                interpretesencillo.InterpreteSencillo.errores.imprimir(interpretesencillo.InterpreteSencillo.horaFecha.format(interpretesencillo.InterpreteSencillo.fecha)+"<br>");
                interpretesencillo.InterpreteSencillo.errores.imprimir("Se Produjo una excepcion debido a un error anterior<br>");
            } catch (IOException ex1) {
                Logger.getLogger(Mientras.class.getName()).log(Level.SEVERE, null, ex1);
            }
            return null;
        }
    }
    
    
}
