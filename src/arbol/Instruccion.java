//Automatas y lenguajes Formales
//Billy Oswaldo Raquec Arias    Carnet 5990-14-5609
//Javier Francisco Sanchez de Leon  Carnet: 5990-06-1924
//interface Instruccion
//Paquete
package arbol;

/**
 * Interfaz que implementan todas las clases que son instrucción y que contiene 
 * todas las acciones (métodos) que pueden realizar todas las instrucciones, en 
 * este caso, todas las instrucciones pueden ser ejecutadas, por lo que todas las
 * clases que implementen esta interfaz están obligadas a tener un método programado
 * para la ejecución.
 * @author billy
 */
public interface Instruccion {
    /**
     * Método que ejecuta la accion propia de la instrucción que se implemente.
     * @param ts Tabla de símbolos del ámbito padre de la sentencia.
     * @return Retorna nulo cuando la sentencia no produce valor al ser ejecutada, 
     * pero cuando se trata de una operación aritmética, una concatenación o una 
     * operación relacional, entonces se devuelve el valor que la operación da como 
     * resultado.
     */
    
    public Object ejecutar(TablaDeSimbolos ts);
}
