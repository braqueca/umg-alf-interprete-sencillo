//Automatas y lenguajes Formales
//Billy Oswaldo Raquec Arias    Carnet 5990-14-5609
//Javier Francisco Sanchez de Leon  Carnet: 5990-06-1924
//Clase Asignacion
//Paquete


package arbol;

import arbol.Simbolo.Tipo;

/**
 * Clase que ejecuta las acciones de una instrucción de asignación y que implementa
 * la interfaz de instrucción
 */
public class Asignacion implements Instruccion{
    /**
     * Identificador de la variable a la que se le asigna el valor.
     */
    private final String id;
    /**
     * Valor que se le asigna a la variable.
     */
    public final Operacion valor;
    
    /**
     * Indica si es una declaracion o solo una asignacion
     */
    public Boolean declaracion=false;
    
    /**
     * Constructor de la clase asignación
     * @param a identificador de la variable
     * @param b valor que se le va a asignar
     */
    public Asignacion(String a, Operacion b) {
        this.id=a;
        this.valor=b;
    }
    /**
     * Método que ejecuta la accion de asignar un valor, es una sobreescritura del 
     * método ejecutar que se debe programar por la implementación de la interfaz
     * instrucción
     * @param ts tabla de símbolos del ámbito padre de la sentencia asignación
     * @return En este caso retorna nulo porque no es una sentencia que genere un valor.
     */
    @Override
    public Object ejecutar(TablaDeSimbolos ts) {
        if(!declaracion){
            ts.setValor(id,valor.ejecutar(ts));
        }
        else{
            ts.add(new Simbolo(id,Tipo.NUMERO));
            ts.setValor(id, valor.ejecutar(ts));
        }
        return null;
    }
    
}
