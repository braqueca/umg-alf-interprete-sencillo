//Automatas y lenguajes Formales
//Billy Oswaldo Raquec Arias    Carnet 5990-14-5609
//Javier Francisco Sanchez de Leon  Carnet: 5990-06-1924
//Clase Declaraciones
//Paquete
package arbol;

import java.util.LinkedList;

/**
 *Clase que ejecuta las acciones de declaraciones y que implementa la interfaz
 * Instruccion
 * @author billyraquec
 */
public class Declaraciones implements Instruccion{
    //lista de Variables a declarar
    private final LinkedList<Instruccion> declaraciones;
    //Constructor que recibe la lista de variables a declarar
    public Declaraciones(LinkedList<Instruccion> declaraciones){
        this.declaraciones=declaraciones;
    }

    /**
     * Metodo que Declara las variables
     * @return retorna null
    */
    
    @Override
    public Object ejecutar(TablaDeSimbolos ts) {
        for(Instruccion in: declaraciones){
            in.ejecutar(ts);
        }
        return null;
    }
    
}
