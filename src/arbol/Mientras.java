//Automatas y lenguajes Formales
//Billy Oswaldo Raquec Arias    Carnet 5990-14-5609
//Javier Francisco Sanchez de Leon  Carnet: 5990-06-1924
//Clase Mientras
//Paquete

package arbol;

import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase que ejecuta las acciones de una instrucción mientras y que implementa
 * la interfaz de instrucción
 */
public class Mientras implements Instruccion{
    /**
     * Condición de la sentencia mientras.
     */
    private final Operacion condicion;
    /**
     * Lista de las instrucciones que deben ejecutarse si la condición se cumple.
     */
    private final LinkedList<Instruccion> listaInstrucciones;
    /**
     * Constructor de la clase Mientras
     * @param a condición que debe evaluarse para ejecutar el ciclo
     * @param b instrucciones que deben ejecutarse si la condición se cumpliera
     */
    public Mientras(Operacion a, LinkedList<Instruccion> b) {
        this.condicion=a;
        this.listaInstrucciones=b;
    }
    /**
     * Método que ejecuta la instrucción mientras, es una sobreescritura del 
     * método ejecutar que se debe programar por la implementación de la interfaz
     * instrucción
     * @param ts tabla de símbolos del ámbito padre de la sentencia
     * @return Esta instrucción retorna nulo porque no produce ningun valor
     */
    @Override
    public Object ejecutar(TablaDeSimbolos ts) {
        try{
            while(Boolean.valueOf(condicion.ejecutar(ts).toString())){
                TablaDeSimbolos tablaLocal=new TablaDeSimbolos();
                tablaLocal.addAll(ts);
                for(Instruccion ins:listaInstrucciones){
                    ins.ejecutar(tablaLocal);
                }
            }
        }catch(Exception ex){
            System.err.println(interpretesencillo.InterpreteSencillo.horaFecha.format(interpretesencillo.InterpreteSencillo.fecha));
            System.err.println("Se Produjo una excepcion debido a un error anterior");
            try {
                interpretesencillo.InterpreteSencillo.errores.imprimir(interpretesencillo.InterpreteSencillo.horaFecha.format(interpretesencillo.InterpreteSencillo.fecha)+"<br>");
                interpretesencillo.InterpreteSencillo.errores.imprimir("Se Produjo una excepcion debido a un error anterior<br>");
            } catch (IOException ex1) {
                Logger.getLogger(Mientras.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return null;
    }   
}