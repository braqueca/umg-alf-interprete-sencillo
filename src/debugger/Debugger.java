//Automatas y lenguajes Formales
//Billy Oswaldo Raquec Arias    Carnet 5990-14-5609
//Javier Francisco Sanchez de Leon  Carnet: 5990-06-1924
//Clase Debugger
//Paquete

package debugger;
//imports
import java.io.FileNotFoundException;   //excepcion de archivo no encontrado
//imports para uso de listas
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;   //import para leer el teclado
import java.util.StringTokenizer;   //import para separar en tokens los strings

public class Debugger {             //Clase debugger
    String path;                    //guardara la direccion del archivo de entrada
    public String comando;          //guardara cada comando
    Scanner lector = new Scanner(System.in);   //creando un Scanner
            
    /**
     * Constructor
     * @param path Direccion al archivo de entrada 
     */
    
    public Debugger(String path){
        this.path=path;
    }
    
    /**
     * Metodo imprimirBienvenida imprime un mensaje de bienvenida
     */
    
    public void imprimirBienvenida() {
        System.out.println("Bienvenido al Debugger");
    }
    
    /**
     * Metodo solicitarComando solicita cada comando
     */
    
    public void solicitarComando() {
        System.out.print(">>>");
        comando = lector.nextLine();
    }
    
    /**
     * Metodo reconocerComando reconoce cada accion del debugger
     * @throws FileNotFoundException
     * @throws Exception 
     */
    
    public void reconocerComando() throws FileNotFoundException, Exception {
        StringTokenizer token = new StringTokenizer(comando);
        
        /*String[] args = null;
        
        */
        
        List <String> args = new ArrayList<>();
        
        
        
        while(token.hasMoreElements()){
           args.add(token.nextToken());
        }
        
        try{
        
            if(args.isEmpty()){
                System.out.println("No ingreso ningun comando");
            }
            
            /*else if(args.get(0).equals("set") && args.get(1).equals("breakpoint")){
                if(args.size()<3){
                    System.out.println("Es necesario colocar la linea para el breakpoint");
                }
                else{
                    System.out.println("Breakpoint asignado en la linea "+args.get(2));
                    breaks.agregar(Integer.parseInt(args.get(2)));
                }
            }
            else if(args.get(0).equals("del") && args.get(1).equals("breakpoint")){
                if(args.size()<3){
                    System.out.println("Es necesario colocar la linea para eliminar breakpont");
                }
                else{
                    if(breakpoints.remove(args.get(2))){
                        System.out.println("Breakpoint eliminado");    
                    }
                    else{
                        System.out.println("No existe el breakpoint");
                    }
                }
            }*/
            else if(args.get(0).equals("print")){
                if(args.size()<2){
                    System.out.println("No ingreso ningun identificador");
                }
                else{ //if(interpretesencillo.InterpreteSencillo.ts.getValor(args.get(1)).toString().equals("Desconocido")){
                    System.out.println(interpretesencillo.InterpreteSencillo.ts.getValor(args.get(1)).toString());
                }
                //else{
                  //  System.out.println("Variable no existe");
                //}
            }
            else if(args.get(0).equals("run")){
                interpretesencillo.InterpreteSencillo.interpretar(path);
                
            }/*
            else if(args.get(0).equals("imprimir")){
                breaks.imprimir();
            }*/
            else{
                System.out.println("Comando no reconocido");
            }
        }catch(IndexOutOfBoundsException ex){
            System.out.println("Comando no reconocido");
        }
        /*File command = new File("comando.txt");
        PrintWriter writer;
        try {
            writer = new PrintWriter(command);
            writer.print(comando);
            writer.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Debugger.class.getName()).log(Level.SEVERE, null, ex);
        }

        Reader leer = new BufferedReader(new FileReader("comando.txt"));

        LexerDebugger lexer = new LexerDebugger(leer);

        parser p = new parser(lexer);

        p.parse();*/
    }
    
}
